const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const bcrypt = require('bcryptjs');
const { check, validationResult } = require('express-validator');
var enableSession; //




/** app initialization */
const app = express();
app.use('/bootstrap',express.static(__dirname+'/node_modules/bootstrap/dist/css'));
app.use('/css',express.static(__dirname+'/views/css'));
app.use('/images',express.static(__dirname+'/views/images'));

/** Body Parser Middleware*/
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());




/** Load View Engine*/
app.set('views',path.join(__dirname,'views'));
app.set('view engine','pug');


/**
 *  Send error in view
 * sendMsg functions
 * @param {string} _temp  pug template
 * @param {boolean} _isAlert  if alert is true
 * @param {Response} _res  response object
 * @param {string} _msg  alert message
 */

var sendMsg = (_temp,_isAlert, _res, _msg , _type='danger')=>{
    _res.render(_temp,{
        isAlert:_isAlert,
        msg : _msg,
        type : _type
    });
};


/**
 * Check if email is correct
 * @param {string} _email
 * @returns {boolean}
 */
var checkEmail=(_email)=>{
    let s = (_email.split('@')[1]).split('.');
    if(s.length ==2)
        return true;
    else
        return false;
};

/**
 *  Initialize db connection
 * @param {string} _dbname name of database
 */
var initConnection = (_dbname)=>{
    /**
     * db connection
     * */
    mongoose.connect('mongodb://localhost/'+_dbname);
    let db = mongoose.connection;

    /**
     *  Check for db errors
     *  */
    db.on('error',(err)=>{
        console.log(err);
    });

    /** check for connection*/
    db.once('open',()=>{
        console.log('Connected to MongoDB');

    });
};

initConnection('dbTroov');
    /**Bind the model*/
let Article = require('./models/article');
let User = require('./models/user');

Article.createCollection().then(()=> {
   console.log('Article Collection is created!');
});
User.createCollection().then(()=> {
   console.log('User Collection is created!');
});



/**
 * Home Route and List of articles
 * */
app.get('/',(req,res)=>{
    if (this.enableSession) {
        Article.find({}, (err, articles) => {
            if (err) {
                    console.log(err);
            } else {
                res.render('index', {
                    title: 'Articles',
                    articles: articles
                });
            }
        });
    } else {
        this.enableSession = false;
        res.redirect('/signin');
    }

});//end



/**
 * Sign in route
 */
app.get('/signin',(req,res)=>{
    res.render('signin');
});//end


/**
 * Sign in submit POST route
 */
app.post('/signin', [
    check('password').isLength({ min: 4 })// password must be at least 4 chars long
    ],(req,res)=> {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        sendMsg('signin',true,res,"Error : Your password is not valid");
    }else {
        if(checkEmail(req.body.username)) {
            User.find({email: req.body.username}, (err, data) => {
                if (data.length != 0) {
                    if (err) {
                        sendMsg('signin', true, res, "Error : Your email is not valid");
                        return;
                    } else {
                        bcrypt.compare(req.body.password, data[0].password, (err, isTrue) => {
                            if (err) {
                                sendMsg('signin', true, res, "Error : Your password is not valid");
                            } else {
                                if (isTrue) {
                                    this.enableSession = true;
                                    res.redirect('/');
                                } else {
                                    sendMsg('signin', true, res, "Error : Your password is not valid");
                                }
                            }
                        });

                    }
                } else {
                    sendMsg('signin', true, res, "You don't have an account please register before ");
                }
            });
        }else {
            sendMsg('signin', true, res, "Error : Your email is not valid");
        }
    }

});//end


/**
 * Register route
 */
app.get('/register',(req,res)=>{
    res.render('register');
});

/**
 * Register submit POST route
 */
app.post('/register', [
    check('email').isEmail(),
    check('password').isLength({ min: 4 })
    ],(req,res)=>{

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        sendMsg('register',true,res,"Error : Your email or password is not valid");
    }else {
        if (checkEmail(req.body.username)) {
            if (req.body.password_confirm == req.body.password) {
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(req.body.password, salt, (err, hash) => {
                        if (err) {
                            sendMsg('register', true, res, "Error : password must be same as the password confirm");
                        } else {
                            let user = new User();
                            user.firstname = req.body.firstname;
                            user.lastname = req.body.lastname;
                            user.username = req.body.username;
                            user.email = req.body.email;
                            user.password = hash;
                            user.save((err) => {
                                if (err) {
                                    console.log(err);
                                    return;
                                } else {
                                    this.enableSession = false;
                                    sendMsg('signin', true, res, "Register success !!", 'success');
                                }
                            });
                        }
                    });
                });
            } else {
                sendMsg('register', true, res, "Error : password must be same as the password confirm");
            }
        }else {
            sendMsg('signin', true, res, "Error : Your email is not valid");
        }
    }
});



/**
 * Add article route
 */
app.get('/article/add',(req,res)=>{
    if(this.enableSession) {
        res.render('article');
    }else {
        this.enableSession=false;
        sendMsg('signin',true,res,"Sign In before" ,'success');
    }
});
/**
 * Add article submit POST route
 */
app.post('/article/add',(req,res)=>{
    if(this.enableSession){

        let article = new Article();
        article.title = req.body.title;
        article.summary = req.body.summary;
        article.category = String(req.body.category);
        article.published = req.body.pubDate;
        article.content = req.body.content;
        article.save((err)=>{
            if(err){
                console.log(err);
            }else {
                res.redirect('/');
            }
        });
    }else {
        this.enableSession=false;
        sendMsg('signin',true,res,"Sign In before" ,'success');
    }
});





/**
 * Update article submit POST route
 */
app.post('/article/update/:id',(req,res)=>{

    let article = {};
    article._id=req.params.id;
    article.title = req.body.title;
    article.summary = req.body.summary;
    article.category = String(req.body.category);
    article.published = Date.parse(req.body.pubDate);
    article.content = req.body.content;
    Article.updateOne({_id:article._id},article,(err)=>{
        if(err){
            console.log(err);
            return;
        }else {
            this.enableSession=true;
            res.redirect('/');
        }
    });
});


/**
 * Re-route article in list to form route
 */
app.get('/article/:id',(req,res)=>{
    if(this.enableSession) {
        Article.findById(req.params.id, (err, article) => {
            if (err) {
                console.log(err);
            } else {
                res.render('article', {
                    article: article
                });
            }

        });
    }else {
        this.enableSession=false;
        sendMsg('signin',true,res,"Sign In before" ,'success');
    }
});


/**
 * Delete article route
 */
app.get('/article/del/:id',(req,res)=>{
    if(this.enableSession) {
        Article.deleteOne({_id: req.params.id}, (err) => {
            if (err) {
                console.log(err);
                return;
            } else {
                res.redirect('/');
            }
        });
    }else {
        this.enableSession=false;
        sendMsg('signin',true,res,"Sign In before" ,'success');
    }
});

/**
 * Sign out route
 */
app.get('/signout',(req,res)=>{
    this.enableSession = false;
    console.log('sign out '+this.enableSession);
    sendMsg('signin',true,res,"Sign out success!" ,'success');

});

/** Start Server*/
app.listen(3000,()=> {
   console.log('server started on port 3000');
   this.enableSession = false;
});
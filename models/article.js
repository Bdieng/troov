let mongoose = require('mongoose');

/** Article schema */

let articleSchema =  mongoose.Schema({
   title:{
       type: String,
       required:true
   },
    summary:{
      type:String,
      required:true
    },
    category:{
      type:String,
      required:true
    },
    published:{
       type: Date,
        required: true
    },
    content:{
       type:String,
        required:true
    }
});

/**
 * Article model
 * @type {Model}
 */

let Article = module.exports = mongoose.model('Article',articleSchema);
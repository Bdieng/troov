let mongoose = require('mongoose');

/** User schema */

let userSchema =  mongoose.Schema({
    username:{
        type: String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    firstname:{
        type:String,
        required:true
    },
    lastname:{
        type: String,
        required: true
    },
    password:{
        type:String,
        required:true
    }
});


/**
 * User model
 * @type {Model}
 */
let User = module.exports = mongoose.model('User',userSchema);
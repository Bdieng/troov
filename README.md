# Troov

----------------------
Command Line Arguments
----------------------

1- Install libraries
    
    npm install
   

2- Run application
    
    npm start (For restart automatically after update in app.js file)
    
3- Listen for connection

    http://localhost:3000/

----------------------
 Project structure
----------------------

- Models

    - User.js 
    - Article.js
    
- Views
    
    - Sign in view 
    - Register view
    - Home & Articles view
    - Form add article view
    
- Controller

    - app.js
    